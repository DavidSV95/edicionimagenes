package imagenes;

import java.awt.Dimension;
import javax.swing.JFrame;

class Ventana extends JFrame {

	public Ventana() {
		Panel panelContenido = new Panel();
		this.setContentPane(panelContenido);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Edición de Imagenes");
		this.setLocation(150, 150);
		this.setPreferredSize(new Dimension(1100, 600));
		this.pack();
		this.setResizable(true);
		this.setVisible(true);
	}
}

