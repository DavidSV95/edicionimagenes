package imagenes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import javax.swing.JLabel;
import javax.swing.JPanel;

class Panel extends JPanel implements ActionListener {

	private JButton  abrirImg, blancoNegro, invertir, grises, rojo, verde, azul, espejo, borde;
	private Imagen IMG;
        private JPanel panelImg1, panelImg2;
        private BufferedImage imagen1, imagen2;
        private JLabel label1, label2;
	public Panel() {
                abrirImg = new JButton("Abrir Imagen");
                blancoNegro = new JButton("Blanco y Negro");
                invertir = new JButton("Invertir");
                grises = new JButton("Escala de grises");
                rojo = new JButton("Filtro rojo");
                verde= new JButton("Filtro verde");
                azul = new JButton("Filtro azul");
                espejo = new JButton("Efecto espejo");
                borde = new JButton("Extracción de bordes");

                abrirImg.addActionListener(this);
		blancoNegro.addActionListener(this);
                invertir.addActionListener(this);
                grises.addActionListener(this); 
                rojo.addActionListener(this); 
                verde.addActionListener(this);
                azul.addActionListener(this);
                espejo.addActionListener(this); 
                borde.addActionListener(this); 
                
                panelImg1 = new JPanel();
                panelImg2 = new JPanel();
                label1 = new JLabel();
                label2 = new JLabel();
                panelImg1.setBorder(BorderFactory.createTitledBorder(
					" Imagen 1 "));;
                panelImg2.setBorder(BorderFactory.createTitledBorder(
					" Imagen 2 " ));;        
                        
                        
		GroupLayout distribuidor = new GroupLayout(this);
		this.setLayout(distribuidor);
		distribuidor.setAutoCreateGaps(true);
		distribuidor.setAutoCreateContainerGaps(true);

		GroupLayout.SequentialGroup secuencial =
				distribuidor.createSequentialGroup();
                GroupLayout.SequentialGroup secuencia2 =
				distribuidor.createSequentialGroup();
		GroupLayout.ParallelGroup paralelo1 =
				distribuidor.createParallelGroup(GroupLayout.Alignment.CENTER);
                GroupLayout.ParallelGroup paralelo2 =
				distribuidor.createParallelGroup(GroupLayout.Alignment.CENTER);
		
                
                secuencial.addComponent(panelImg1);        
                paralelo1.addComponent(panelImg1);   
                secuencial.addComponent(panelImg2);        
                paralelo1.addComponent(panelImg2);  
                
                secuencia2.addComponent(abrirImg);        
                paralelo2.addComponent(abrirImg); 
                secuencia2.addComponent(blancoNegro);        
                paralelo2.addComponent(blancoNegro); 
                secuencia2.addComponent(invertir);        
                paralelo2.addComponent(invertir); 
                secuencia2.addComponent(grises);
                paralelo2.addComponent(grises);
                secuencia2.addComponent(rojo);
                paralelo2.addComponent(rojo);
                secuencia2.addComponent(verde); 
                paralelo2.addComponent(verde);
                secuencia2.addComponent(azul); 
                paralelo2.addComponent(azul);
                secuencia2.addComponent(espejo); 
                paralelo2.addComponent(espejo);
                secuencia2.addComponent(borde); 
                paralelo2.addComponent(borde);
                        
		distribuidor.setHorizontalGroup(
				distribuidor.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addGroup(secuencial)
                                        .addGroup(secuencia2)
                );
		distribuidor.setVerticalGroup(
				distribuidor.createSequentialGroup()
					.addGroup(paralelo1)
                                        .addGroup(paralelo2)
                );
                
                panelImg1.add(label1);
                panelImg2.add(label2);
               
	}
            

	public void actionPerformed(ActionEvent ev) {
            
            
                 
                if (ev.getSource() == abrirImg){
                IMG = new Imagen();
                imagen1 = IMG.abrirImagen();
		label1.setIcon(new ImageIcon(imagen1));
                }
                if (ev.getSource() == blancoNegro){
                imagen2 = IMG.blancoNegro(imagen1, 128);
		label2.setIcon(new ImageIcon(imagen2));    
                }
                if (ev.getSource() == invertir){
                imagen2 = IMG.invertirImagen(imagen1);
		label2.setIcon(new ImageIcon(imagen2));       
                }

                if (ev.getSource() == grises){
                imagen2 = IMG.escalaGrises(imagen1);
		label2.setIcon(new ImageIcon(imagen2));       
                }
                if (ev.getSource() == rojo ){
                imagen2 = IMG.filtroRojo(imagen1);
		label2.setIcon(new ImageIcon(imagen2));       
                }
                if (ev.getSource() == verde){
                imagen2 = IMG.filtroVerde(imagen1);
		label2.setIcon(new ImageIcon(imagen2));       
                }
                if (ev.getSource() == azul){
                imagen2 = IMG.filtroAzul(imagen1);
		label2.setIcon(new ImageIcon(imagen2));       
                }
                if (ev.getSource() == espejo ){
                imagen2 = IMG.espejo(imagen1);
		label2.setIcon(new ImageIcon(imagen2));       
                }
                if (ev.getSource() == borde){
                imagen2 = IMG.bordes(imagen1);
		label2.setIcon(new ImageIcon(imagen2));       
                }
                             
                }
	
}
