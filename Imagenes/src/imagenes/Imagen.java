package imagenes;



import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Imagen {
  
    private BufferedImage imagen;
    private Color auxColor;
    private int mediaColor;
    
    public BufferedImage abrirImagen(){
        imagen=null;

        JFileChooser selector=new JFileChooser();
        selector.setDialogTitle("Seleccione una imagen");

        FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter("JPG & GIF & BMP", "jpg", "gif", "bmp");
        selector.setFileFilter(filtroImagen);
     
        int flag=selector.showOpenDialog(null);
        if(flag==JFileChooser.APPROVE_OPTION){
            try {
                File imagenSeleccionada=selector.getSelectedFile();
                imagen = ImageIO.read(imagenSeleccionada);
            } catch (Exception e) {
            }
                 
        }     
        return imagen;
    }
    
        private int colorRGBaSRGB(Color colorRGB){
        int colorSRGB;
        colorSRGB=(colorRGB.getRed() << 16) | (colorRGB.getGreen() << 8) | colorRGB.getBlue();
        return colorSRGB;
    }
    private BufferedImage clonarBufferedImage(BufferedImage bufferImage){
        BufferedImage copiaImagen=new BufferedImage (bufferImage.getWidth(),bufferImage.getHeight(),bufferImage.getType());
        copiaImagen.setData(bufferImage.getData());
        return copiaImagen;
    }
    private int calcularMediaColor(Color color){
        int media;
        media=(int)((color.getRed()+color.getGreen()+color.getBlue())/3);
        return media;
    }
 
        
    public BufferedImage blancoNegro(BufferedImage imagen, int umbral){
        imagen = this.clonarBufferedImage(imagen);
        for( int i = 0; i < imagen.getWidth(); i++ ){
            for( int j = 0; j < imagen.getHeight(); j++ ){
                auxColor=new Color(imagen.getRGB(i, j));
                if (this.calcularMediaColor(auxColor)>=umbral){
                   auxColor=new Color(255,255,255, auxColor.getAlpha());
                }else{
                   auxColor=new Color(0, 0, 0, auxColor.getAlpha());
                 }    
                imagen.setRGB(i, j,this.colorRGBaSRGB(auxColor));
            }
        }
       
                
        return imagen;
    }
    
    public BufferedImage escalaGrises(BufferedImage imagen){
        imagen=this.clonarBufferedImage(imagen);
        for( int i = 0; i < imagen.getWidth(); i++ ){
            for( int j = 0; j < imagen.getHeight(); j++ ){
                auxColor=new Color(imagen.getRGB(i, j));
                mediaColor=this.calcularMediaColor(auxColor);
                imagen.setRGB(i, j,this.colorRGBaSRGB(new Color(mediaColor,mediaColor,mediaColor,auxColor.getAlpha())));
            }
        }
        
        return imagen;
    }
    
    public BufferedImage invertirImagen(BufferedImage imagen){
        imagen=this.clonarBufferedImage(imagen);
        for( int i = 0; i < imagen.getWidth(); i++ ){
            for( int j = 0; j < imagen.getHeight(); j++ ){
                auxColor=new Color(imagen.getRGB(i, j));
                imagen.setRGB(i, j,this.colorRGBaSRGB(new Color(255-auxColor.getRed(), 
                        255-auxColor.getGreen(), 255-auxColor.getBlue(), auxColor.getAlpha())));
            }
        }
       
        return imagen;
    }
    
    public BufferedImage filtroRojo(BufferedImage imagen){
        imagen=this.clonarBufferedImage(imagen);
        for( int i = 0; i < imagen.getWidth(); i++ ){
            for( int j = 0; j < imagen.getHeight(); j++ ){
               auxColor=new Color(imagen.getRGB(i, j));
               mediaColor=this.calcularMediaColor(auxColor);
               imagen.setRGB(i, j,this.colorRGBaSRGB(new Color(mediaColor,0,0,auxColor.getAlpha())));
            }
        }
       
        return imagen;
    }
    
    public BufferedImage filtroVerde(BufferedImage imagen){
        imagen=this.clonarBufferedImage(imagen);
        for( int i = 0; i < imagen.getWidth(); i++ ){
            for( int j = 0; j < imagen.getHeight(); j++ ){
               auxColor=new Color(imagen.getRGB(i, j));
               mediaColor=this.calcularMediaColor(auxColor);
               imagen.setRGB(i, j,this.colorRGBaSRGB(new Color(0,mediaColor,0,auxColor.getAlpha())));
            }
        }
       
        return imagen;
    }
    
    public BufferedImage filtroAzul(BufferedImage imagen){
        imagen=this.clonarBufferedImage(imagen);
        for( int i = 0; i < imagen.getWidth(); i++ ){
            for( int j = 0; j < imagen.getHeight(); j++ ){
               auxColor=new Color(imagen.getRGB(i, j));
               mediaColor=this.calcularMediaColor(auxColor);
               imagen.setRGB(i, j,this.colorRGBaSRGB(new Color(0,0,mediaColor,auxColor.getAlpha())));
            }
        }
       
        return imagen;
    }
    
    public BufferedImage espejo(BufferedImage imagen){
        imagen=this.clonarBufferedImage(imagen);
        BufferedImage auxImagen=this.clonarBufferedImage(imagen);
        int auxAncho;
        auxAncho = imagen.getWidth()-1;
        for( int i = 0; i < imagen.getWidth(); i++ ){
            for( int j = 0; j < imagen.getHeight(); j++ ){    
                auxImagen.setRGB(auxAncho, j,imagen.getRGB(i, j));
            }
            auxAncho = auxAncho-1;
        }
        
        return auxImagen;
    }
    
    public BufferedImage bordes(BufferedImage imagen){
        imagen=this.clonarBufferedImage(imagen);

        CannyEdgeDetector detector = new CannyEdgeDetector();

        detector.setLowThreshold(1f);
        detector.setHighThreshold(6f);

        detector.setSourceImage(imagen);
        detector.process();
        imagen = detector.getEdgesImage();
        
        return imagen;
    }
    
    

    
}
