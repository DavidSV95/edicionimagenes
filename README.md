# README #

EdicionImagenes es un proyecto de edición de imágenes, en el que podemos abrir una imagen y realizar distintas operaciones y efectos en la imagen.

Para realizar este proyecto hemos usado el entorno NetBeans y como lenguaje de programación JAVA, el trabajo con imágenes mediante las librerías Swing (para apertura de cuadros de dialogo, ventanas y selección de imágenes) , imageIO (para lectura y creación en disco de una imagen), BufferedImage (permite poder trabajar con las imágenes) y Color (permite operar con los 3 componentes RGB).

El proyecto consta de 5 clases:

* Principal.java que contiene el main en el que se ejecuta la aplicación.

* Ventana.java que extiende de JFrame que es la crea y establece los parámetros de la ventana de la aplicación.

* Panel.java que extiende de JPanel y es la que establece los distintos paneles y botones de la aplicación, con sus respectivos eventos (ActionEvent) y llamadas a los métodos de la clase Imagen.java dependiendo del botón pulsado. Dichos paneles y botones se ordenan mediante GroupLayout.

* Imagen.java contiene los distintos métodos para aplicar las transformaciones en la imagen.

* CannyEdgeDetector.Java implementa el algoritmo de Canny.